from django.core.exceptions import ValidationError
from django.core.validators import validate_email
from django.http import HttpResponse, JsonResponse
from django.shortcuts import get_object_or_404, render
from django.template.loader import render_to_string

from gifts.gifts.models import Holiday


def holiday_view(request, key):
    """Просмотр праздника"""
    holiday = get_object_or_404(Holiday.objects_open, key=key)
    free_gifts = holiday.gift_set.filter(reserved=False)
    return render(request, 'gifts/holiday.html', {'holiday': holiday, 'free_gifts': free_gifts})




def discard_view(request, key, gift_id):
    """Cнятие брони
    :param request: HttpRequest Запрос
    :param key: секретный ключ праздника
    :param gift_id: идентификатор подарка
    """
    if request.method != 'POST':
        # HTTP-код 405 означает, что метод не разрешен
        # В нашем случае мы принимает только POST-запросы на изменение состояния
        # брони подарка/
        # http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html#sec10.4.6
        return HttpResponse(status=405)

    # Ищем запршенный проздник
    holiday = get_object_or_404(Holiday, key=key, opened=True)
    # Ищем подарок
    gift = get_object_or_404(holiday.gift_set, pk=gift_id)

    data = request.POST
    action = data.get('action', 'reserve')
    if action != 'discard':
        # HTTP-код 400 Bad Request говорит, что сервер не смог понять запрос
        # В нашем случае - неверное действие
        # http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html#sec10.4.1
        return HttpResponse(status=400)

    # Валидация данных может быть сделана с помощью форм, но в данном примере
    # выполняется "ручная" проверка без форм.

    valid = True  # Полагаем изначально, что данные корректные
    msg = ''  # Сообщение, которое будем показывать пользователю

    name = data.get('name', '').strip()  # Убираем пробелы из начала и конца
    email = data.get('email', '')
    try:
        validate_email(email)
    except ValidationError:
        valid = False
        msg = 'Введите корректный e-mail'

    if not valid:
        return JsonResponse({'msg': msg})

    # Данные верные, можно выполнять соответствующее действие

    # Перд выполнением действия необходимо убедиться, что состояние бронирования
    # подарка не изменилось, т.к. пока один посетитель смотрел страницу
    # и выбирал подходящий подарок, другой его уже мог изменить бронь.
    if action == 'discard':
        if gift.reserved:
            if gift.reserve_email != email:
                msg = 'Подарок забронирован на другой e-mail'
            else:
                gift.reserve_email = ''
                gift.reserve_name = ''
                gift.save()
        else:
            msg = 'Подарок не забронирован'

    # Если есть сообщение, значит, что-то пошло не так
    if msg:
        return JsonResponse({'msg': msg})

    # Сообщения нет, всё в порядке.
    # Рендерим новое состояние подарка и отдаем клиенту.
    html = render_to_string('gifts/include/gift.html',
                            {'gift': gift, 'holiday': holiday})
    return JsonResponse({'msg': '', 'html': html})

def reserve_view(request, key, gift_id):
    """Бронирование подарка и снятие брони
    :param request: HttpRequest Запрос
    :param key: секретный ключ праздника
    :param gift_id: идентификатор подарка
    """
    if request.method != 'POST':
        # HTTP-код 405 означает, что метод не разрешен
        # В нашем случае мы принимает только POST-запросы на изменение состояния
        # брони подарка/
        # http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html#sec10.4.6
        return HttpResponse(status=405)
    # Ищем запршенный проздник
    holiday = get_object_or_404(Holiday, key=key, opened=True)
    # Ищем подарок
    gift = get_object_or_404(holiday.gift_set, pk=gift_id)
    data = request.POST
    action = data.get('action', 'reserve')
    if action != 'reserve':
        # HTTP-код 400 Bad Request говорит, что сервер не смог понять запрос
        # В нашем случае - неверное действие
        # http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html#sec10.4.1
        return HttpResponse(status=400)
    # Валидация данных может быть сделана с помощью форм, но в данном примере
    # выполняется "ручная" проверка без форм.

    valid = True  # Полагаем изначально, что данные корректные
    msg = ''  # Сообщение, которое будем показывать пользователю
    name = data.get('name', '').strip()  # Убираем пробелы из начала и конца
    email = data.get('email', '')
    try:
        validate_email(email)
    except ValidationError:
        valid = False
        msg = 'Введите корректный e-mail'
    if not valid:
        return JsonResponse({'msg': msg})
    # Данные верные, можно выполнять соответствующее действие
    # Перд выполнением действия необходимо убедиться, что состояние бронирования
    # подарка не изменилось, т.к. пока один посетитель смотрел страницу
    # и выбирал подходящий подарок, другой его уже мог изменить бронь.
    if action == 'reserve':
        if gift.reserved:
            msg = 'Подарок уже забронирован'
        else:
            gift.reserve_email = email
            gift.reserve_name = name
            gift.save()

    # Если есть сообщение, значит, что-то пошло не так
    if msg:
        return JsonResponse({'msg': msg})
    # Сообщения нет, всё в порядке.
    # Рендерим новое состояние подарка и отдаем клиенту.
    html = render_to_string('gifts/include/gift.html',
                            {'gift': gift, 'holiday': holiday})
    return JsonResponse({'msg': '', 'html': html})
