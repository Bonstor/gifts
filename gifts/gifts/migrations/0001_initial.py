# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Gift',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('name', models.CharField(max_length=150, verbose_name='Название')),
                ('order', models.IntegerField(default=10, verbose_name='Порядок следования')),
                ('reserved', models.BooleanField(default=False, verbose_name='Забронирован')),
                ('reserve_email', models.CharField(blank=True, max_length=100, verbose_name='E-mail', null=True)),
                ('reserve_name', models.CharField(blank=True, max_length=100, verbose_name='Имя', null=True)),
            ],
            options={
                'ordering': ['order', 'name'],
                'verbose_name_plural': 'Подарки',
                'verbose_name': 'Подарок',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Holiday',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('name', models.CharField(max_length=100, verbose_name='Название')),
                ('date', models.DateField(default=django.utils.timezone.now, verbose_name='Дата проведения')),
                ('opened', models.BooleanField(help_text='Может быть просмотрен через сайт', default=True, verbose_name='Открыт')),
                ('key', models.CharField(max_length=32, verbose_name='Ключ доступа')),
            ],
            options={
                'ordering': ['date'],
                'verbose_name_plural': 'Праздники',
                'verbose_name': 'Праздник',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='gift',
            name='holiday',
            field=models.ForeignKey(verbose_name='Праздник', to='gifts.Holiday'),
            preserve_default=True,
        ),
    ]
